#Sardana repository moved to GitHub

The official Sardana repo is now located in:

https://github.com/sardana-org/sardana

The Repository in SourceForge is obsolete and will be kept empty.

Hint: You probably should update your git remotes with the following command:

```
git remote set-url origin https://github.com/sardana-org/sardana.git
```

